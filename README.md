# deepr for data·café

<h1 align="center">
	<img src="https://raw.githubusercontent.com/deeprjs/deepr/%40deepr/runtime%400.4.5/branding/deepr-logo-with-tagline.svg" width="350" alt="Deepr — A specification for invoking remote methods, deeply!">
	<br>
</h1>

## Reasons under the hood

data·café is a open-source data fabric, hightly generic, based on lots of independant bricks. Each of them need to communicate with others in a very efficient way.

## Wiki

Goto our [wiki](https://gitlab.com/data-cafe/deepr/-/wikis/home)

- deepr [v.0.4.5 specification](https://gitlab.com/data-cafe/deepr/-/wikis/deepr-specification-0.4.5) compiling samples and explainations found in the [deepr](https://github.com/deeprjs/deepr) repository at tag `0.4.5`.
- deepr [v.1 specicication](https://gitlab.com/data-cafe/deepr/-/wikis/deepr-specification-1.0.0) proposal (draft)
  - [compare](https://gitlab.com/data-cafe/deepr/-/wikis/deepr-migration-0.4.5-to-1.0.0) actual v.0.4.5 with v.1
- deepr [status](deepr-status)

## Origin: deepr project

Our work is based on [deepr](https://github.com/deeprjs/deepr) project and tried to improve this work.

More information are availabe in [readme](https://github.com/deeprjs/deepr) with samples and in [runtime](https://github.com/deeprjs/deepr/tree/master/packages/runtime) section including kind of specs.

The original [licence](https://github.com/deeprjs/deepr/blob/master/LICENSE) is MIT, and the logo had [credits](https://github.com/deeprjs/deepr#logo)

## License

We're publishing this repo, like data·café, under [AGPL v.3](LICENCE), to support open-source and avoid unfair monetization of the community.
